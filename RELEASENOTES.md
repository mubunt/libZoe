# RELEASE NOTES:  *libZoe*, a library providing an API for developing simple text-based environments.
___
Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.4.10**:
  - Updated build system components.

- **Version 1.4.9**:
  - Fixed typo in README file.

- **Version 1.4.8**:
  - Updated build system.

- **Version 1.4.7**:
  - Removed unused files.

- **Version 1.4.6**:
  - Updated build system component(s)

- **Version 1.4.5**:
  - Renamed executable built as example as *example_xxx* to be compliant with other projects.

- **Version 1.4.4**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.4.3**:
  - Some minor changes in .comment file(s).

- **Version 1.4.2**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.4.1**:
  - Updated makefiles and asociated README information.

- **Version 1.4.0**:
  - Added object *ZOE_OBJECT_ANSWER* for the characteristics of the forms handled by **libZoe**. That impacts open and close questions.
  - Redesigned file selector. It looks now like a close question and allows to go up the named directory.
  - Centralized memory allocation and free routines to be able to check the adequacy between both.
  - Added new api routine **zoe_getnextfreeline()**.
  - Redeclared *char* parameters as *const char* in api.
  - Enriched the *zoe_init* function with a new parameter specifying whether a user display area ("Log" accessible via stdout / stder) is necessary.

- **Version 1.3.0**:
  - Renamed project as **libZoe**.

- **Version 1.2.4**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.2.3**:
  - Added tagging of new release.

- **Version 1.2.2**:
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Moved from GPL v2 to GPL v3.

- **Version 1.2.1**:
    - Replaced debug compilation options bt release ones.
    - Changed library name from *zoe.a* to *libzoe.a*.

- **Version 1.2.0**:
    - Added Progress bar form.

- **Version 1.1.0**:
    - Added File/Directory chooser form.
    - Slightly modified List form display.

- **Version 1.0.0**:
    - First release.

- **Versions 0.1.0 to 0.5.0**:
	- Development versions to ignore.
