//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// CONSTANTS
//------------------------------------------------------------------------------
#define OBJECTID_INCREMENT	25
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _zoe_initObject( void ) {
	objectID_table = (struct objectID_item *) _zoe_calloc((size_t) objectID_size, sizeof(struct objectID_item));
	for (objectID i = 0; i < objectID_size; i++) {
		objectID_table[i].startline = objectID_table[i].startcolumn = -1;
		objectID_table[i].endline = objectID_table[i].endcolumn = -1;
	}
}
//------------------------------------------------------------------------------
void _zoe_endObject( void ) {
	_zoe_free(objectID_table);
}
//------------------------------------------------------------------------------
objectID _zoe_newObject( int startLine, int startColumn, int endLine, int endColumn ) {
	objectID i;
	for (i = 0; i < objectID_size; i++) {
		if (objectID_table[i].startline == -1) break;
	}
	if (i == objectID_size) {
		objectID_table = realloc(objectID_table, sizeof(struct objectID_item) * (size_t) (objectID_size + OBJECTID_INCREMENT));
		i = objectID_size;
		objectID_size = objectID_size + (objectID) OBJECTID_INCREMENT;
	}
	objectID_table[i].startline = startLine;
	objectID_table[i].startcolumn = startColumn;
	objectID_table[i].endline = endLine;
	objectID_table[i].endcolumn = endColumn;
	objectID_table[i].addtional_information_1 = 0;
	objectID_table[i].addtional_information_2 = 0;
	return i;
}
//------------------------------------------------------------------------------
void _zoe_updateObject( objectID id, int endLine ) {
	objectID_table[id].endline = endLine;
}
//------------------------------------------------------------------------------
void _zoe_storeInfoObject( objectID id, int info1, int info2 ) {
	objectID_table[id].addtional_information_1 = info1;
	objectID_table[id].addtional_information_2 = info2;
}
//------------------------------------------------------------------------------
void _zoe_clearObject( objectID id ) {
	size_t n = (size_t) (objectID_table[id].endcolumn - objectID_table[id].startcolumn + 1);
	char *spaces = _zoe_malloc(n + 1, sizeof(char));
	*spaces = '\0';
	for (size_t i = 0; i < n; i++) strcat(spaces, " ");
	for (int line = objectID_table[id].startline; line <= objectID_table[id].endline; line++) {
		if (line <= (int) screenmapside) DISPLAY_AT(line, objectID_table[id].startcolumn, "%s", spaces);
		_zoe_removeScreenmap(id, line);
	}
	objectID_table[id].startline = objectID_table[id].startcolumn = -1;
	objectID_table[id].endline = objectID_table[id].endcolumn = -1;
	_zoe_free(spaces);
	_zoe_refresh();
}
//------------------------------------------------------------------------------
boolean _zoe_retrieveObject( objectID id, struct objectID_item *object ) {
	if (id < 0 || id >= objectID_size) return FALSE;
	if (objectID_table[id].startline == -1) return FALSE;
	object->startline = objectID_table[id].startline;
	object->startcolumn = objectID_table[id].startcolumn;
	object->endline = objectID_table[id].endline;
	object->endcolumn = objectID_table[id].endcolumn;
	object->addtional_information_1 = objectID_table[id].addtional_information_1;
	object->addtional_information_2 = objectID_table[id].addtional_information_2;
	return TRUE;
}
//------------------------------------------------------------------------------
// ZOE API FUNCTIONS
//------------------------------------------------------------------------------
void zoe_clearObject( objectID id ) {
	if (id < 0 || id >= objectID_size) return;
	if (objectID_table[id].startline == -1) return;
	int freeline, c;
	_zoe_getCursorPosition( &freeline, &c);
	ENTER_ZOE();
	_zoe_clearObject(id);
	EXIT_ZOE(freeline);
}
//==============================================================================
