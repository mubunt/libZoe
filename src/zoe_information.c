//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static const char *error_title		= "APPLICATION ERROR";
static const char *message_title	= "APPLICATION MESSAGE";
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void readChoice( struct mapping_generic map ) {
	_zoe_setTerminalMode(MODE_RAW);
	boolean selection = TRUE;
	boolean help_is_displayed = FALSE;
	objectID idhelp;
	HIDE_CURSOR();
	while (1) {
		int n = _zoe_incomingKey();
		if (help_is_displayed) {
			_zoe_clearObject(idhelp);
			help_is_displayed = FALSE;
		}
		switch (n) {
		case _help:
			_zoe_help(&idhelp, map.helpline, map.helpstartcolumn, map.helpendcolumn, 1, (char *) help_information);
			help_is_displayed = TRUE;
			break;
		case _ok:
			n = ZOE_OK;
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (n == ZOE_OK) break;
	}
	SHOW_CURSOR();
	_zoe_setTerminalMode(MODE_ECHOED);
}
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
void zoe_information( objectID *id, const char *label ) {
	struct mapping_generic map;
	int freeline, c;
	_zoe_getCursorPosition( &freeline, &c);
	ENTER_ZOE();
	// The width of the error is the smallest between 2/3 of terminal width and the width
	// of the label plus edges (2 * 2 characters)
	size_t labellen = _zoe_length_of_longest_line(label);
	size_t width= (size_t) MIN( (2 * nbCOLS / 3), (int) labellen + 4);
	// Position of the error
	_zoe_map_error(label, (int) width, 4, NULL, &map);
	// Object recording
	*id = _zoe_newObject(map.line, map.column, 0, map.column + (int) width + 1);
	// Display message
	int line = map.line;
	_zoe_displayTop(*id, &line, map.column, width, attributs.information.bold, attributs.information.color, (char *) "");
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.information.bold, attributs.information.color);
	_zoe_displayLabel(*id, &line, map.column, width - 4, label, attributs.information.bold, attributs.information.color);
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.information.bold, attributs.information.color);
	_zoe_displayBottom(*id, line, map.column,  width, attributs.information.bold, attributs.information.color);
	FLUSH();
	_zoe_updateObject(*id, map.line + frameNbLines - 1);
	EXIT_ZOE(freeline);
}
//------------------------------------------------------------------------------
void zoe_message( const char *label ) {
	struct mapping_generic map;
	int freeline, c;
	_zoe_getCursorPosition( &freeline, &c);
	ENTER_ZOE();
	// The width of the error is the smallest between 2/3 of terminal width and the width
	// of the label plus edges (2 * 2 characters)
	size_t labellen =  _zoe_length_of_longest_line(label);
	size_t width= (size_t) MIN( (2 * nbCOLS / 3), (int) labellen + 4);
	width = MAX(width, strlen(message_title));
	// Position of the error (7 = frame + title + empty lines + button)
	_zoe_map_error(label, (int) width, 6, (char *)help_information, &map);
	// Object recording
	objectID id = _zoe_newObject(map.line, map.column, 0, map.column + (int) width + 1);
	// Display message
	int line = map.line;
	_zoe_displayTop(id, &line, map.column, width, attributs.message.bold, attributs.message.color, (char *) message_title);
	_zoe_displayEmptyLine(id, &line, map.column, width, attributs.message.bold, attributs.message.color);
	_zoe_displayLabel(id, &line, map.column, width - 4, label, attributs.message.bold, attributs.message.color);
	_zoe_displayEmptyLine(id, &line, map.column, width, attributs.message.bold, attributs.message.color);
	_zoe_displayOK(id, &line, map.column, width, attributs.message.bold, attributs.message.color);
	_zoe_displayEmptyLine(id, &line, map.column, width, attributs.message.bold, attributs.message.color);
	_zoe_displayBottom(id, line, map.column,  width, attributs.message.bold, attributs.message.color);
	FLUSH();
	_zoe_updateObject(id, map.line + frameNbLines - 1);
	// Read selection
	readChoice(map);
	// Clear
	_zoe_clearObject(id);
	EXIT_ZOE(freeline);
}
//------------------------------------------------------------------------------
void zoe_error( const char *label ) {
	struct mapping_generic map;
	int freeline, c;
	_zoe_getCursorPosition( &freeline, &c);
	ENTER_ZOE();
	// The width of the error is the smallest between 2/3 of terminal width and the width
	// of the label plus edges (2 * 2 characters)
	size_t labellen = _zoe_length_of_longest_line(label);
	size_t width= (size_t) MIN( (2 * nbCOLS / 3), (int) labellen + 4);
	// With an adaptation if the title is longer !!!
	width = MAX(width, strlen(error_title) + 6);
	// Position of the error (7 = frame + title + empty lines + button)
	_zoe_map_error(label, (int) width, 6, (char *)help_information, &map);
	// Object recording
	objectID id = _zoe_newObject(map.line, map.column, 0, map.column + (int) width + 1);
	// Display error
	int line = map.line;
	_zoe_displayTop(id, &line, map.column, width, attributs.error.bold, attributs.error.color, (char *) error_title);
	_zoe_displayEmptyLine(id, &line, map.column, width, attributs.error.bold, attributs.error.color);
	_zoe_displayLabel(id, &line, map.column, width - 4, label, attributs.error.bold, attributs.error.color);
	_zoe_displayEmptyLine(id, &line, map.column, width, attributs.error.bold, attributs.error.color);
	_zoe_displayOK(id, &line, map.column, width, attributs.error.bold, attributs.error.color);
	_zoe_displayEmptyLine(id, &line, map.column, width, attributs.error.bold, attributs.error.color);
	_zoe_displayBottom(id, line, map.column,  width, attributs.error.bold, attributs.error.color);
	FLUSH();
	_zoe_updateObject(id, map.line + frameNbLines - 1);
	// Read selection
	readChoice(map);
	// Clear
	_zoe_clearObject(id);
	EXIT_ZOE(freeline);
}
//==============================================================================
