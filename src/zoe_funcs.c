//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define CTRL_A				0x01
#define CTRL_H				0x08
#define BS 					0x08
#define CR 					0x0d
#define ESCAPE				0x1b
#define ARROW_UP			0x41
#define ARROW_DOWN			0x42
#define ARROW_RIGHT			0x43
#define ARROW_LEFT			0x44
#define OPENBRACKET			0x5b
#define TILDA 				0x7e
#define DEL 				0x7f
#define FATALPREFIX	"(ZOE) "
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _zoe_fatal( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	zoe_end();
	fprintf(stderr, "\n" START_COLOR "%s%s" STOP_COLOR "\n\n", FOREGROUND(red), boldon, FATALPREFIX, buff);
	exit(EXIT_FAILURE);
}

void _zoe_warning( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	int l, c;
	_zoe_getCursorPosition(&l, &c);
	MOVE_CURSOR(__lineinit, 1);
	fprintf(stdout, START_COLOR "%s%s" STOP_COLOR "\n", FOREGROUND(yellow), boldon, FATALPREFIX, buff);
	++__lineinit;
	MOVE_CURSOR(l, c);
	FLUSH();
}
//------------------------------------------------------------------------------
char *_zoe_malloc( size_t number, size_t size) {
	char *p;
	if (NULL == (p = malloc(number * size)))
		_zoe_fatal("%s", "No more memory space avalaible for allocation. Abort.");
	*p = '\0';
	++_allocated_memory;
	return p;
}

char *_zoe_calloc( size_t nmemb, size_t size ) {
	char *p;
	if (NULL == (p = calloc(nmemb, size)))
		_zoe_fatal("%s", "No more memory space avalaible for allocation. Abort.");
	*p = '\0';
	++_allocated_memory;
	return p;
}

char *_zoe_strdup( const char *s ) {
	char *p;
	if (NULL == (p = strdup(s)))
		_zoe_fatal("%s", "No more memory space avalaible for allocation. Abort.");
	++_allocated_memory;
	return p;
}

char *_zoe_realpath( const char *s ) {
	char *p;
	if (NULL == (p = realpath(s, NULL)))
		_zoe_fatal("%s", "No more memory space avalaible for allocation. Abort.");
	++_allocated_memory;
	return p;
}

void _zoe_free( void *p ) {
	if (p != NULL) {
		free(p);
		--_allocated_memory;
	}
}
//------------------------------------------------------------------------------
size_t _zoe_visiblestrlen( const char *str ) {
	size_t n = 0;
	const char *pt = str;
	while (*pt) {
		if ((unsigned int) *pt == 0xffffffc3) ++n;
		++pt;
	}
	return(strlen(str) - n);
}
//------------------------------------------------------------------------------
void _zoe_getTerminalSize( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w))
		_zoe_fatal("%s", "Cannot get terminal size Abort!");
	*lines = w.ws_row;
	*cols = w.ws_col;
}
//------------------------------------------------------------------------------
void _zoe_getCursorPosition( int *line, int *column) {
	char buf[64];
	char cmd[]=GET_CURSOR;
	struct termios save, raw;

	memset((void *)&save, 0, sizeof(save));
	memset((void *)&raw, 0, sizeof(raw));
	tcgetattr(0, &save);
	cfmakeraw(&raw);
	tcsetattr(0, TCSANOW, &raw);
	ssize_t notused = write(1, cmd, sizeof(cmd));
	notused = read (0, buf, sizeof(buf));
	tcsetattr(0, TCSANOW, &save);
	sscanf(buf + 2,"%d;%dR", line, column);
}
//------------------------------------------------------------------------------
void _zoe_setTerminalMode( terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &cooked)) {
			_zoe_fatal("%s", "Cannot set the parameter 'MODE_ECHOED' associated with the terminal. Abort!");
		}
		break;
	case MODE_RAW:
		if (0 != tcgetattr(STDIN_FILENO, &cooked)) {
			_zoe_fatal("%s", "Cannot set the parameter 'MODE_RAW' associated with the terminal. Abort!");
		}
		raw = cooked;
		cfmakeraw(&raw);
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &raw)) {
			_zoe_fatal("%s", "Cannot set the parameter 'MODE_RAW/TCSANOW' associated with the terminal. Abort!");
		}
		break;
	}
}
//------------------------------------------------------------------------------
void _zoe_getTerminalCapabilities( void ) {
#define	TERMBUF_SIZE		2048
#define	TERMSBUF_SIZE		1024
#define	DEFAULT_TERM		"unknown"
	// Find out what kind of terminal this is.
	char *term;
	char termbuf[TERMBUF_SIZE];
	if ((term = getenv("TERM")) == NULL) term = (char *)DEFAULT_TERM;
	// Load information relative to the terminal from the erminfo database.
	switch (tgetent(termbuf, term)) {
	case -1:
		_zoe_fatal("%s", "The terminfo database could not be found. Abort!");
		break;
	case 0:
		_zoe_fatal("%s", "There is no entry for this terminal in the terminfo database. Abort!");
		break;
	default:
		break;
	}
	// Get various string-valued capabilities.
	static char sbuf[TERMSBUF_SIZE];
	char *sp = sbuf;
	if (NULL == (screeninit = tgetstr("ti", &sp)))
		_zoe_fatal("%s", "There is no entry 'ti' (screen init) for this terminal in the terminfo database. Abort!");
	if (NULL == (screendeinit = tgetstr("te", &sp)))
		_zoe_fatal("%s", "There is no entry 'te' (screen deinit) for this terminal in the terminfo database. Abort!");
	if (NULL == (clear = tgetstr("cl", &sp)))
		_zoe_fatal("%s", "There is no entry 'cl' (clear screen) for this terminal in the terminfo database. Abort!");
	if (NULL == (move = tgetstr("cm", &sp)))
		_zoe_fatal("%s", "There is no entry 'cm' (cursor move) for this terminal in the terminfo database. Abort!");
}
//------------------------------------------------------------------------------
void _zoe_displayLogTitle( void ) {
#define	LOGTITLE 			"  OUTPUT"
	if (firstLogLine != -1) {
		MOVE_CURSOR_AND_CLEAR_LINE(firstLogLine - 1, 1);
		DISPLAY_AT(firstLogLine - 1, 1, "%s%s", REVERSE_BOLD, LOGTITLE);
		for (int i = (int) strlen(LOGTITLE); i < nbCOLS; i++) DISPLAY(" ");
		DISPLAY("%s", STOP_COLOR);
		SET_TOP_AND_BOTTOM_LINES(firstLogLine, nbLINES);
		MOVE_CURSOR(firstLogLine, 1);
	}
}
//------------------------------------------------------------------------------
int _zoe_incomingKey( void ) {
	int c = getchar();
	switch (c) {
	case BS:
	case DEL:
		c = _back_previous_question;
		break;
	case CR:
		c = _ok;
		break;
	case CTRL_A:
		c = _help;
		break;
	case ESCAPE:
		switch (getchar()) {
		case OPENBRACKET:
			switch (getchar()) {
			case ARROW_RIGHT:
				c = _go_next_question;
				break;
			case ARROW_LEFT:
				c = _back_previous_question;
				break;
			case ARROW_DOWN:
				c = _arrow_down;
				break;
			case ARROW_UP:
				c = _arrow_up;
				break;
			case '3':
				switch (getchar()) {
				case TILDA:
					c = _abort_session;
					break;
				default:
					c = _not_known;
					break;
				}
				break;
			default:
				c = _not_known;
				break;
			}
			break;
		default:
			c = _not_known;
			break;
		}
		break;
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		c = c & 0xf;
		break;
	default:
		c = _not_known;
	}
	return(c);
}
//------------------------------------------------------------------------------
int _zoe_incomingChar( void ) {
	int c = getchar();
	switch (c) {
	case BS:
	case DEL:
		c = _backspace;
		break;
	case CR:
		c = _ok;
		break;
	case CTRL_A:
		c = _help;
		break;
	case ESCAPE:
		switch (getchar()) {
		case OPENBRACKET:
			switch (getchar()) {
			case ARROW_RIGHT:
				c = _go_next_question;
				break;
			case ARROW_LEFT:
				c = _back_previous_question;
				break;
			case '3':
				switch (getchar()) {
				case TILDA:
					c = _abort_session;
					break;
				default:
					c = _not_known;
					break;
				}
				break;
			default:
				c = _not_known;
				break;
			}
			break;
		default:
			c = _not_known;
			break;
		}
		break;
	default:
		break;
	}
	return(c);
}
//------------------------------------------------------------------------------
size_t _zoe_count_characters_out_escaped_codes( const char *str ) {
	unsigned char *s = (void *)str;
	size_t non_escape_chars = 0;
	size_t non_printable_nbchars = 0;
	int state = 0;
	while (1) {
		if (*s == '\0') break;
		switch (state) {
		case 0:
			if (*s == '\033') state = 1;
			else if (*s >= 128) ++non_printable_nbchars;
			else ++non_escape_chars;
			break;
		case 1:
			if (*s == '[') state = 2;
			break;
		case 2:
			if (*s >= '0' && *s <= '9') state = 3;
			else if (*s == 'm') state = 0;
			break;
		case 3:
			if (*s >= '0' && *s <= '9') state = 4;
			else if (*s == ';') state = 2;
			break;
		case 4:
			if (*s == 'm') state = 0;
			else if (*s == ';') state = 2;
			break;
		}
		++s;
	}
	return non_escape_chars + non_printable_nbchars / 3;
}
//------------------------------------------------------------------------------
size_t _zoe_compute_index_out_escaped_codes( const char *str, size_t idx) {
	unsigned char *s = (void *)str;
	size_t non_escape_chars = 0;
	size_t escape_chars = 0;
	size_t non_printable_nbchars = 0;
	int state = 0;
	while (1) {
		if (idx == non_escape_chars + non_printable_nbchars / 3 && (non_printable_nbchars % 3) == 0)
			break;
		if (*s == '\0')
			break;
		switch (state) {
		case 0:
			if (*s == '\033') {
				++escape_chars;
				state = 1;
			} else if (*s >= 128) ++non_printable_nbchars;
			else ++non_escape_chars;
			break;
		case 1:
			if (*s == '[') {
				++escape_chars;
				state = 2;
			}
			break;
		case 2:
			if (*s >= '0' && *s <= '9') {
				++escape_chars;
				state = 3;
			} else if (*s == 'm') {
				++escape_chars;
				state = 0;
			}
			break;
		case 3:
			if (*s >= '0' && *s <= '9') {
				++escape_chars;
				state = 4;
			} else if (*s == ';') {
				++escape_chars;
				state = 2;
			}
			break;
		case 4:
			if (*s == 'm') {
				++escape_chars;
				state = 0;
			} else if (*s == ';') {
				++escape_chars;
				state = 2;
			}
			break;
		}
		++s;
	}
	return non_escape_chars + escape_chars + non_printable_nbchars;
}
//------------------------------------------------------------------------------
int _zoe_how_many_lines( const char *info, size_t width ) {
	const char *ptb = info;
	size_t n, m;
	int k = 0;
	while (1) {
		++k;
		if (_zoe_count_characters_out_escaped_codes(ptb) > (size_t) width) {
			n =_zoe_compute_index_out_escaped_codes(ptb, (size_t) width);
			size_t i;
			for (i = 0; i < n; i++)
				if (ptb[i] == '\n') break;
			if (ptb[i] == '\n') {
				n = i + 1;
			} else {
				i = n - 1;
				while (ptb[i] != '\n' && ptb[i] != ' ' && i != 0) --i;
				if (i != 0) n = i + 1;
			}
		} else {
			size_t i = 0;
			while (ptb[i] != '\n' && ptb[i] != '\0') ++i;
			if (ptb[i] == '\n')
				n = i + 1;
			else
				n = strlen(ptb);
		}
		ptb += n;
		if (ptb >= (info + strlen(info))) break;
	}
	return k;
}
//------------------------------------------------------------------------------
size_t _zoe_length_of_longest_line( const char *text ) {
	char *ptext = _zoe_strdup(text);
	char *pt = ptext;
	char *pstart = ptext;
	size_t len = 0;
	while (*pt != '\0') {
		if (*pt == '\n') {
			*pt = '\0';
			len = MAX(len, strlen(pstart));
			*pt = '\n';
			pstart = pt + 1;
		}
		++pt;
	}
	len = MAX(len, strlen(pstart));
	_zoe_free(ptext);
	return len;
}
//------------------------------------------------------------------------------
void _zoe_displayTop( objectID id, int *line, int column, size_t width, zoe_bold a, _color c, const char *title ) {
	frameNbLines = 1;
	char buffer[MAXLINE];
	size_t length = width - 2;
	sprintf(buffer, START_COLOR "%s", a, c, C_ULC);
	for (size_t i = 0; i < length; i++) strcat(buffer, C_H);
	strcat(buffer, C_URC);
	strcat(buffer, STOP_COLOR);
	_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
	if (strlen(title) != 0) {
		size_t n = (width - strlen(title)) / 2 - 2;
		sprintf(buffer, START_COLOR "%s%*s%s%*s%s", a, c, C_V, (int) n, " ", title, (int) (width - strlen(title) - n - 2), " ", C_V);
		_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
		++frameNbLines;
		sprintf(buffer, START_COLOR "%s", a, c, D_VHR);
		for (size_t i = 0; i < length; i++) strcat(buffer, D_H);
		strcat(buffer, D_VHL);
		strcat(buffer, STOP_COLOR);
		_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
		++frameNbLines;
	}
}

void _zoe_displayBottom( objectID id, int line, int column, size_t width, zoe_bold a, _color c ) {
	++frameNbLines;
	char buffer[MAXLINE];
	size_t length = width - 2;
	sprintf(buffer, START_COLOR "%s", a, c, C_BLC);
	for (size_t i = 0; i < length; i++) strcat(buffer, C_H);
	strcat(buffer, C_BRC);
	strcat(buffer, STOP_COLOR);
	_zoe_recordScreenmap(id, line, column, buffer, TODISPLAY);
}

void _zoe_displayEmptyLine( objectID id, int *line, int column, size_t width, zoe_bold a,_color c ) {
	++frameNbLines;
	char buffer[MAXLINE];
	size_t length = width - 2;
	sprintf(buffer, START_COLOR "%s", a, c, C_V);
	for (size_t i = 0; i < length; i++) strcat(buffer, " ");
	strcat(buffer, C_V);
	strcat(buffer, STOP_COLOR);
	_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
}

void _zoe_displayLabel( objectID id, int *line, int column, size_t width, const char *label, zoe_bold bold, _color color ) {
	char buffer[MAXLINE];
	size_t m = MAX(width, strlen(label));
	char *buffline = _zoe_malloc(m + 1, sizeof(char));
	const char *ptlabel = label;
	size_t n;

	while (1) {
		if (_zoe_count_characters_out_escaped_codes(ptlabel) > (size_t) width) {
			n =_zoe_compute_index_out_escaped_codes(ptlabel, (size_t) width);
			size_t i;
			for (i = 0; i < n; i++)
				if (ptlabel[i] == '\n') break;
			if (ptlabel[i] == '\n') {
				n = i + 1;
			} else {
				i = n - 1;
				while (ptlabel[i] != '\n' && ptlabel[i] != ' ' && i != 0) --i;
				if (i != 0) n = i + 1;
			}
			strncpy(buffline, ptlabel, n);
			buffline[n - 1] = '\0';
		} else {
			size_t i = 0;
			while (ptlabel[i] != '\n' && ptlabel[i] != '\0') ++i;
			if (ptlabel[i] == '\n')
				n = i + 1;
			else
				n = strlen(ptlabel);
			strcpy(buffline, ptlabel);
		}
		m = _zoe_count_characters_out_escaped_codes(buffline);
		if (m < (size_t) width)
			for (size_t i = m; i < width; i++) strcat(buffline, " ");
		++frameNbLines;
		sprintf(buffer, START_COLOR "%s %-*s %s" STOP_COLOR, bold, color, C_V, (int) width, buffline, C_V);
		_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
		ptlabel += n;
		if (ptlabel >= (label + strlen(label))) break;
	}
	_zoe_free(buffline);
}

void _zoe_displayOK( objectID id, int *line, int column, size_t width, zoe_bold a, _color c ) {
#define OK 	"  OK  "
	char okhighlighted[MAXLINE];
	char buf[16];
	sprintf(buf, STOP_COLOR START_COLOR, a, c);
	size_t p = 4 * (width - strlen(OK) - 2) / 5;
	sprintf(okhighlighted, START_COLOR "%s", a, c, C_V);
	for (size_t i = 0; i < p; i++) strcat(okhighlighted, " ");
	strcat(okhighlighted, REVERSE);
	strcat(okhighlighted, OK);
	strcat(okhighlighted, buf);
	for (size_t i = 0; i < width - p - strlen(OK) - 2; i++) strcat(okhighlighted, " ");
	strcat(okhighlighted, C_V);
	strcat(okhighlighted, STOP_COLOR);

	++frameNbLines;
	_zoe_recordScreenmap(id, (*line)++, column, okhighlighted, TODISPLAY);
}
//------------------------------------------------------------------------------
boolean _zoe_isDirectory( char *file ) {
	struct stat sb;
	if (file == NULL) return(FALSE);
	if (lstat(file, &sb) < 0) return(FALSE);
	mode_t mode = sb.st_mode & S_IFMT;
	if (mode == S_IFDIR) return(TRUE);
	return(FALSE);
}

boolean _zoe_isFile( char *file ) {
	struct stat sb;
	if (file == NULL) return(FALSE);
	if (lstat(file, &sb) < 0) return(FALSE);
	mode_t mode = sb.st_mode & S_IFMT;
	if (mode == S_IFDIR) return(FALSE);
	return(TRUE);
}
//------------------------------------------------------------------------------
static int compare(const void  *a, const void *b) {
	return strcmp(* (char *const *) a, * (char *const *) b);
}

static const char *get_file_suffix(const char *filename) {
	const char *dot = strrchr(filename, '.');
	if(!dot) return "";
	return dot + 1;
}

#define CHECK_SUFFIX(list, name)	if (list == NULL || mode == S_IFDIR) \
										found  = TRUE; \
									else { \
										const char *suff = get_file_suffix(name); \
										found = FALSE; \
										for (int i = 0; list[i][0] != '\0'; i++) \
											if (0 == strcmp(list[i], suff)) { found = TRUE; break; } \
									}


struct tree_scanner *_zoe_treeScanner(const char *rootdir, const char *currentdir,
                                      zoe_choose type, const char *suffix[], boolean hiddenfile) {
	DIR *pDir;
	struct dirent *pDirent;
	struct stat sb;
	char object[PATH_MAX];
	boolean found;
	char *root, *directory;

	root = _zoe_realpath(rootdir);
	directory = _zoe_realpath(currentdir);

	if ((pDir = opendir(directory))  == NULL)
		_zoe_fatal("Cannot open directory '%s'", directory);
	// Step 1: Count the number of directories and regular files.
	size_t nbObjects = 0;
	size_t maxLenObjects = MAX(strlen(UPDIR), strlen(directory));
	size_t maxVisibleLenObjects = MAX(_zoe_visiblestrlen(UPDIR), _zoe_visiblestrlen(directory));
	while ((pDirent = readdir(pDir)) != NULL) {
		if (strlen(pDirent->d_name) == 0) continue;
		if (strncmp(pDirent->d_name, ".", 2) == 0) continue;
		if (strncmp(pDirent->d_name, "..", 3) == 0) continue;
		if (pDirent->d_name[0] == '.' && ! hiddenfile) continue;
		sprintf(object, "%s/%s", directory, pDirent->d_name);
		if (lstat(object, &sb) < 0) _zoe_fatal("Cannot stat %s\n", object);
		mode_t mode = sb.st_mode & S_IFMT;
		if (mode == S_IFDIR || (mode != S_IFDIR && type != CHOOSE_DIR)) {
			CHECK_SUFFIX(suffix, pDirent->d_name);
			if (found) {
				++nbObjects;
				maxLenObjects = MAX(maxLenObjects, strlen(pDirent->d_name));
				maxVisibleLenObjects = MAX(maxVisibleLenObjects, _zoe_visiblestrlen(pDirent->d_name));
			}
		}
	}
	rewinddir(pDir);
	// Step 2: Allocate structure and table for directories and regular files and fill them.
	struct tree_scanner *pttree_scanner = (struct tree_scanner *) _zoe_malloc(1, sizeof(struct tree_scanner));
	pttree_scanner->maxLenObjects = maxLenObjects + 1;
	pttree_scanner->maxVisibleLenObjects = maxVisibleLenObjects + 1;
	pttree_scanner->nbObjects = nbObjects;
	if (strcmp(root, directory) != 0) ++pttree_scanner->nbObjects;	// +1 for ".." added later
	pttree_scanner->ptObjects = (char **) _zoe_malloc(pttree_scanner->nbObjects, sizeof(char *));
	size_t indObject = 0;
	while ((pDirent = readdir(pDir)) != NULL) {
		if (strlen(pDirent->d_name) == 0) continue;
		if (strncmp(pDirent->d_name, ".", 2) == 0) continue;
		if (strncmp(pDirent->d_name, "..", 3) == 0) continue;
		if (pDirent->d_name[0] == '.' && ! hiddenfile) continue;
		sprintf(object, "%s/%s", directory, pDirent->d_name);
		lstat(object, &sb);
		mode_t mode = sb.st_mode & S_IFMT;
		if (mode == S_IFDIR || (mode != S_IFDIR && type != CHOOSE_DIR)) {
			CHECK_SUFFIX(suffix, pDirent->d_name);
			if (found) {
				pttree_scanner->ptObjects[indObject] = _zoe_malloc(pttree_scanner->maxLenObjects + 2, sizeof(char));
				// 1 for hypothetic "/" to be added, 1 for "\0"
				if  (mode == S_IFDIR) {
					char s[PATH_MAX];
					sprintf(s, "%s/", pDirent->d_name);
					strcpy(pttree_scanner->ptObjects[indObject], s);
				} else
					strcpy(pttree_scanner->ptObjects[indObject], pDirent->d_name);
				++indObject;
			}
		}
	}
	if (strcmp(root, directory) != 0) {
		pttree_scanner->ptObjects[indObject] = _zoe_malloc(pttree_scanner->maxLenObjects + 2, sizeof(char));
		strcpy(pttree_scanner->ptObjects[indObject], UPDIR);
	}
	closedir(pDir);
	// Step 3: Sort these tables alphabetically
	qsort(pttree_scanner->ptObjects, pttree_scanner->nbObjects, sizeof(char *), compare);
	_zoe_free(root);
	_zoe_free(directory);
	return(pttree_scanner);
}

void _zoe_free_treeScanner( struct tree_scanner *pt ) {
	for (size_t i = 0; i < pt->nbObjects; i++)
		_zoe_free(pt->ptObjects[i]);
	_zoe_free(pt->ptObjects);
	_zoe_free(pt);
}
//==============================================================================
