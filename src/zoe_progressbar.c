//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int progressbarLine;
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void refresh_bar( objectID id, int *line, int column, size_t width,  int visupercent, int percent) {
	char buffer[MAXLINE], buffer2[MAXLINE];
	sprintf(buffer, START_COLOR "%s %3d%% " STOP_COLOR START_COLOR,
	        attributs.progressbar.bold, attributs.progressbar.color, C_V, percent,
	        attributs.bar.bold, attributs.bar.color);
	for (int i = 0; i < visupercent; i++) strcat(buffer, SIMPLEBLOCK);
	strcat(buffer, STOP_COLOR);

	sprintf(buffer2, START_COLOR, attributs.progressbar.bold, attributs.progressbar.color);
	for (int i = visupercent; i < (int) width; i++) strcat(buffer2, SIMPLELIGHTSHADE);
	strcat(buffer2, " ");
	strcat(buffer2, C_V);
	strcat(buffer2, STOP_COLOR);

	strcat(buffer, buffer2);
	_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
}

static void display_bar( objectID id, int *line, int column, size_t width ) {
	frameNbLines += 1;
	progressbarLine = *line;
	refresh_bar(id, line, column, width, 0, 0);
}
//------------------------------------------------------------------------------
// ZOE API FUNCTIONS
//------------------------------------------------------------------------------
void zoe_progressbar( objectID *id, const char *label ) {
	struct mapping_generic map;
	int freeline, c;
	_zoe_getCursorPosition( &freeline, &c);
	ENTER_ZOE();
	// The width of the form is the  3/4 of terminal
	size_t width= (size_t) (3 * nbCOLS / 4);
	size_t withbar = width - 9;
	// Position of the error
	_zoe_map_error(label, (int) width, 6, NULL, &map);
	// Object recording
	*id = _zoe_newObject(map.line, map.column, 0, map.column + (int) width + 1);
	// Display message
	int line = map.line;
	_zoe_displayTop(*id, &line, map.column, width, attributs.progressbar.bold, attributs.progressbar.color, (char *) "");
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.progressbar.bold, attributs.progressbar.color);
	_zoe_displayLabel(*id, &line, map.column, width - 4, label, attributs.progressbar.bold, attributs.progressbar.color);
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.progressbar.bold, attributs.progressbar.color);
	display_bar(*id, &line, map.column,withbar);
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.progressbar.bold, attributs.progressbar.color);
	_zoe_displayBottom(*id, line, map.column,  width, attributs.progressbar.bold, attributs.progressbar.color);
	FLUSH();
	_zoe_updateObject(*id, map.line + frameNbLines - 1);
	_zoe_storeInfoObject(*id, (int) withbar, progressbarLine);
	EXIT_ZOE(freeline);
}
//------------------------------------------------------------------------------
void zoe_progressbar_display( objectID id, int percent ) {
	int freeline, c;
	struct objectID_item object;
	_zoe_getCursorPosition( &freeline, &c);
	ENTER_ZOE();
	if (percent < 0) percent = 0;
	if (percent > 100) percent = 100;
	if (_zoe_retrieveObject(id, &object)) {
		int currentpercent = object.addtional_information_1 * percent / 100;
		refresh_bar(id, &object.addtional_information_2, object.startcolumn, (size_t) object.addtional_information_1, currentpercent, percent);
	}
	EXIT_ZOE(freeline);
}
//==============================================================================
