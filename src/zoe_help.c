//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
#include "zoe_help.h"
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _zoe_help( objectID *id, int line, int startcolumn, int endcolumn, int num_args, ... ) {
	va_list argp;
	char *help;

	size_t width = (size_t) (endcolumn - startcolumn + 1);
	*id = _zoe_newObject(line, startcolumn, 0, endcolumn + 1);
	_zoe_displayTop(*id, &line, startcolumn, width, helpbold, helpcolor, (char *)"");
	va_start(argp, num_args);
	for(int i = 0; i < num_args; i++) {
		char *help = va_arg(argp, char *);
		_zoe_displayLabel(*id, &line, startcolumn, width - 4, help, helpbold, helpcolor);
	}
	va_end(argp);
	_zoe_displayBottom(*id, line, startcolumn, width, helpbold, helpcolor);
	_zoe_updateObject(*id, line);
	FLUSH();
}
//==============================================================================
