//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void display_answer( objectID id, struct mapping_open map,  size_t lengthanwser, char *answer ) {
	char buffer[MAXLINE];
	char buff2[64];
	int col = map.answercolumn;
	int line = 0;
	int lastl = line;
	int lastc = col - 1;
	sprintf(buffer, START_COLOR, attributs.answer.bold, attributs.answer.color);
	sprintf(buff2, START_COLOR, attributs.question.bold, attributs.question.color);
	char *pt = buffer + strlen(buffer);
	for (size_t i = 0; i < lengthanwser; i++) {
		if (col > nbCOLS) {
			_zoe_recordScreenmap(id, map.line + line, map.answercolumn, buffer, TODISPLAY);
			++line;
			col = map.answercolumn;
			if (i >= strlen(answer))
				sprintf(buffer, START_COLOR, attributs.question.bold, attributs.question.color);
			else
				sprintf(buffer, START_COLOR, attributs.answer.bold, attributs.answer.color);
			pt = buffer + strlen(buffer);
		}
		if (i == strlen(answer))
			strcat(buffer, buff2);
		if (i >= strlen(answer))
			strcat(buffer, ANSWER);
		else {
			*pt = answer[i];
			++pt;
			*pt = '\0';
			lastl = line;
			lastc = col;
		}
		++col;
	}
	/*
		boolean colorongoing = TRUE;
		for (size_t i = 0; i < lengthanwser; i++) {
			if (col > nbCOLS) {
				_zoe_recordScreenmap(id, map.line + line, map.answercolumn, buffer, TODISPLAY);
				++line;
				col = map.answercolumn;
				sprintf(buffer, START_COLOR, attributs.answer.bold, attributs.answer.color);
				pt = buffer + strlen(buffer);
			}
			if (i >= strlen(answer)) {
				if (colorongoing) {
					strcat(buffer, buff2);
					colorongoing = FALSE;
				}
				strcat(buffer, ANSWER);
			} else {
				*pt = answer[i];
				++pt;
				*pt = '\0';
				lastl = line;
				lastc = col;
			}
			++col;
		}
	*/
	strcat(buffer, STOP_COLOR);
	_zoe_recordScreenmap(id, map.line + line, map.answercolumn, buffer, TODISPLAY);
	if (lastc == nbCOLS)
		MOVE_CURSOR(map.line + lastl + 1, map.answercolumn);
	else
		MOVE_CURSOR(map.line + lastl, lastc + 1);
	FLUSH();
}
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
int zoe_open_question( objectID *id, int line, int column,
                       const char *question, const char *help, char *answer, size_t lengthanwser ) {
	struct mapping_open map;
	char buffer[MAXLINE];
	ENTER_ZOE();
	// Position of the question / answer / help
	_zoe_map_open(line, column, strlen(question), lengthanwser, (char *) help_open_question, help, &map);
	// Get objject ID
	*id = _zoe_newObject(map.line, column, map.line + map.answer_additional_line_needed, nbCOLS);
	// Print Question and answer area
	sprintf(buffer, START_COLOR "%s ", attributs.question.bold, attributs.question.color, question);
	_zoe_recordScreenmap(*id, map.line, column, buffer, TODISPLAY);
	display_answer(*id, map, lengthanwser, (char *) "");
	// Read answer
	_zoe_setTerminalMode(MODE_RAW);
	MOVE_CURSOR(map.line, map.answercolumn);
	*answer = '\0';
	char *ptanswer = answer;
	int n, answerLine = 0;
	boolean help_is_displayed = FALSE;
	objectID idhelp;
	int answerCol = map.answercolumn;
	while (1) {
		n = _zoe_incomingChar();
		if (help_is_displayed) {
			_zoe_clearObject(idhelp);
			MOVE_CURSOR(map.line + answerLine, answerCol);
		}
		switch (n) {
		case _not_known:
			DISPLAY_BELL();
			break;
		case _backspace:
			if (answerCol == map.answercolumn && answerLine == 0) {
				n = ZOE_BACK;
				*answer = '\0';
			} else {
				if (answerCol == map.answercolumn) {
					--answerLine;
					answerCol = nbCOLS;
				} else
					--answerCol;
				--ptanswer;
				*ptanswer = '\0';
				display_answer(*id, map, lengthanwser, answer);
			}
			break;
		case _help:
			_zoe_help(&idhelp, map.helpline, map.helpstartcolumn, map.helpendcolumn, 2, (char *)help_open_question, help);
			help_is_displayed = TRUE;
			MOVE_CURSOR(map.line + answerLine, answerCol);
			break;
		case _abort_session:
			n = ZOE_ABORT;
			break;
		case _back_previous_question:
			n = ZOE_BACK;
			break;
		case _ok:
			n = ZOE_OK;
			break;
		default:
			if (answerCol == nbCOLS) {
				++answerLine;
				answerCol = map.answercolumn;
			}
			*ptanswer = (char) n;
			++ptanswer;
			*ptanswer = '\0';
			display_answer(*id, map, lengthanwser, answer);
			++answerCol;
			if (strlen(answer) == lengthanwser) n = ZOE_OK;
			break;
		}
		if (n == ZOE_OK || n == ZOE_ABORT || n == ZOE_BACK) break;
	}
	if (n == ZOE_BACK) _zoe_clearObject(*id);
	_zoe_setTerminalMode(MODE_ECHOED);
	EXIT_ZOE(map.line + map.answer_additional_line_needed + 1);
	return n;
}
//==============================================================================
