//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
int						_allocated_memory	= 0;		// Number of allocated memories still to be deleted
zoe_log 				_logmode			= WITH_LOG;	// WITH_LOG or WITHOUT_LOG
int 					needed_lines		= 0;
int 					needed_columns		= 0;
int 					nbLINES				= 0;
int 					nbCOLS				= 0;
int 					firstLogLine		= 0;
int 					firstQuestLine		= 0;
int 					lastQuestLine		= 0;
int 					_next_free_line		= 0;
objectID 				objectID_size		= 20;
struct objectID_item 	*objectID_table		= NULL;
struct zoe_attributs	attributs;
_color					helpcolor;
zoe_bold				helpbold;
struct screenmap_item 	**screenmap			= NULL;
size_t 					screenmapside		= 0;
__sighandler_t 			initial_signal;
int 					frameNbLines;
char 					*screeninit;			// Startup terminal initialization
char 					*screendeinit;			// Exit terminal de-initialization
char 					*clear;					// Clear screen
char 					*move;					// Cursor positioning
int 					__lineinit			= 0;
int 					__colinit			= 0;
boolean					__screeninit		= FALSE;
FILE 					*fdlog				= NULL;
char					*titleCopy			= NULL;
__sighandler_t 			current_int_signal;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void cutOutScreen( void ) {
	// Cutting out the screen (top: title,text, question and answer parts. Bottom: user log part)
	firstQuestLine = 3;
	if (_logmode == WITH_LOG) {
		lastQuestLine = firstQuestLine + needed_lines - 1;
		firstLogLine = lastQuestLine + 2;
		if (nbLINES < firstLogLine)
			firstLogLine = -1;
	} else {
		needed_lines = nbLINES - 3;
		needed_columns = nbCOLS;
		lastQuestLine = firstQuestLine + needed_lines - 1;
		firstLogLine = -1;
	}
}

static void sizemanagement( int sig ) {
	signal(SIGWINCH, SIG_IGN);
	// Get cursor position
	int line, col;
	_zoe_getCursorPosition(&line, &col);
	// Get terminal size
	int lines, cols;
	_zoe_getTerminalSize(&lines, &cols);
	if (lines < needed_lines || cols < needed_columns) {
		_zoe_warning("The terminal must have at least %d lines and %d columns.", needed_lines, needed_columns);
		do {
			_zoe_warning("Currently %d lines and %d columns.",lines, cols);
			sleep(5);
			_zoe_getTerminalSize(&lines, &cols);
		} while (lines < needed_lines || cols < needed_columns);
		_zoe_warning("%s", "That's fine.");
	}
	nbLINES = lines;
	nbCOLS = cols;
	// Refresh
	cutOutScreen();
	_zoe_title(titleCopy);
	_zoe_displayLogTitle();
	_zoe_refresh();
	MOVE_CURSOR(line, col);
	FLUSH();
	signal(SIGWINCH, sizemanagement);
}
//------------------------------------------------------------------------------
// ZOE API FUNCTIONS
//------------------------------------------------------------------------------
void zoe_init( int lines_needed, int columns_needed, zoe_log log ) {
	current_int_signal = signal(SIGINT, SIG_IGN);
	// Save parameters
	needed_lines = lines_needed + 2;	// Title: 2 lines
	needed_columns = columns_needed;
	_logmode = log;
	// Predefined color
	attributs.title.color = FOREGROUND(white);
	attributs.question.color = FOREGROUND(white);
	attributs.menu.color = FOREGROUND(white);
	attributs.gonogo.color = FOREGROUND(white);
	attributs.list.color = FOREGROUND(white);
	attributs.chooser.color = FOREGROUND(white);
	attributs.text.color = FOREGROUND(white);
	attributs.error.color = FOREGROUND(white);
	attributs.message.color = FOREGROUND(white);
	attributs.information.color = FOREGROUND(white);
	attributs.answer.color = FOREGROUND(white);
	helpcolor = FOREGROUND(white);
	// Predefined style
	attributs.title.bold = boldon;
	attributs.question.bold = boldoff;
	attributs.menu.bold = boldoff;
	attributs.gonogo.bold = boldoff;
	attributs.list.bold = boldoff;
	attributs.chooser.bold = boldoff;
	attributs.text.bold = boldoff;
	attributs.error.bold = boldon;
	attributs.message.bold = boldoff;
	attributs.information.bold = boldoff;
	attributs.answer.bold = boldoff;
	helpbold = boldoff;
	// Some initializations to avoid a crash if there is an accidental stop
	screenmapside = 0;
	objectID_table = NULL;
	titleCopy = NULL;
	// Ignore window size changes
	initial_signal = signal(SIGWINCH, SIG_IGN);
	// Get terminal capabilities (instead of using escape code)
	_zoe_setTerminalMode(MODE_RAW);
	_zoe_getTerminalCapabilities();
	// Get terminal size
	_zoe_getTerminalSize(&nbLINES, &nbCOLS);
	if (nbLINES < needed_lines)
		_zoe_fatal("The terminal must have at least %d lines. Currently %d lines. Abort!", needed_lines, nbLINES);
	if (nbCOLS < needed_columns)
		_zoe_fatal("The terminal must have at least %d columns. Currently %d columns. Abort!", needed_columns, nbCOLS);
	// Save initial state of the terminal screen to be restore on end
	SAVEINITSCREEN(nbLINES);
	// Cout out the screen
	cutOutScreen();
	// Just to check if any trouble may occur....
	_zoe_setTerminalMode(MODE_ECHOED);
	// Init object management
	_zoe_initObject();
	// Initialize screen map (image of the question part of the screen)
	screenmapside = (size_t) lastQuestLine;
	_zoe_initScreenmap();
	_zoe_set_next_free_line(1);
	// Focus on log part
	_zoe_displayLogTitle();
	FLUSH();
	// Change size signal management
	signal(SIGWINCH, sizemanagement);
	signal(SIGINT, current_int_signal);
}
//------------------------------------------------------------------------------
void zoe_end( void ) {
	current_int_signal = signal(SIGINT, SIG_IGN);
	if (titleCopy != NULL) _zoe_free(titleCopy);
	_zoe_setTerminalMode(MODE_ECHOED);
	// Clear object management
	_zoe_endObject();
	// Clear refresh mechanism
	_zoe_endScreenmap();
	// Restore initial signal
	signal(SIGWINCH, initial_signal);
	// Restore initial screnn state
	RESTOREINITSCREEN(nbLINES);
	FLUSH();
	signal(SIGINT, current_int_signal);
	// Check memories allocatted then freed
	if (_allocated_memory) {
		if (_allocated_memory > 0)
			fprintf(stderr, "\n" START_COLOR "WARNING: Allocated memories... there are still %d memory blocks allocated and not freed" STOP_COLOR "\n\n", FOREGROUND(red), boldon, _allocated_memory);
		else
			fprintf(stderr, "\n" START_COLOR "WARNING: Allocated memories... there were %d freed memory blocks that were not allocated!" STOP_COLOR "\n\n", FOREGROUND(red), boldon, abs(_allocated_memory));
		fflush(stderr);
	}
}
//==============================================================================
