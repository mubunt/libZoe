//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int firstMenuLine = 0;
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define HIGHLIGHT(n)	sprintf(buffer, START_COLOR "%s %s%*d %-*s" STOP_COLOR START_COLOR " %s" STOP_COLOR, \
    						attributs.menu.bold, attributs.menu.color, C_V, REVERSE, (int)idxwidth, n + 1, (int)valwidth, values[n], attributs.menu.bold, attributs.menu.color, C_V); \
						_zoe_recordScreenmap(*id, firstMenuLine + n, map.column, buffer, TODISPLAY);
#define UNHIGHLIGHT(n)	sprintf(buffer, START_COLOR "%s %*d %-*s %s" STOP_COLOR, \
							attributs.menu.bold, attributs.menu.color, C_V, (int)idxwidth, n + 1, (int)valwidth, values[n], C_V); \
						_zoe_recordScreenmap(*id, firstMenuLine + n, map.column, buffer, TODISPLAY);
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void displayBodyMenu( objectID id, int *line, int column,  const char *values[], size_t nvalues,
                             size_t idxw, size_t valw, zoe_bold bold, _color color ) {
	char buffer[MAXLINE];
	frameNbLines += (int) nvalues;
	for (int i = 0; i < (int) nvalues; i++) {
		sprintf(buffer, START_COLOR "%s %*d %-*s %s" STOP_COLOR,
		        bold, color, C_V, (int) idxw, i + 1, (int)valw, values[i], C_V);
		_zoe_recordScreenmap(id, *line + i, column, buffer, TODISPLAY);
	}
	*line += (int) nvalues;
}
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _zoe_displayMenu(objectID id, int line, int column, const char *values[], size_t nvalues,
                      size_t width, size_t idxw, size_t valw, const char *title, zoe_bold bold, _color color ) {
	_zoe_displayTop(id, &line, column, width, bold, color, title);
	firstMenuLine = line;
	displayBodyMenu(id, &line, column, values, nvalues, idxw, valw, bold, color);
	_zoe_displayBottom(id, line, column, width, bold, color);
	FLUSH();
}
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
int zoe_menu( objectID *id, int line, int column, const char *values[], size_t nvalues,  const char *title) {
	struct mapping_generic map;
	char buffer[MAXLINE];
	ENTER_ZOE();
	// The width of the menu is the largest between the widths (1) of the values and the title (2),
	// plus the edges (2 * 2 characters) and the index of values (idxwidth characters + 1) (3)
	size_t valwidth = 0;
	for (size_t i = 0; i < nvalues; i++) valwidth = MAX(valwidth, strlen(values[i]));	// (1)
	valwidth = MAX(valwidth, strlen(title));											// (2)
	size_t idxwidth = (size_t) ((nvalues / 10) + ((nvalues % 10) ? 1 : 0));
	size_t maxwidth = valwidth + idxwidth + 5;											// (3)
	// Position of the question / answer / menu / help
	_zoe_map_menu(line, column, nvalues, (char *)help_menu, &map);
	// Get object id.
	*id = _zoe_newObject(map.line, map.column, 0, map.column + (int) maxwidth + 1);
	// Display of the menu
	_zoe_displayMenu(*id, map.line, map.column, values, nvalues, maxwidth, idxwidth, valwidth, title, attributs.menu.bold, attributs.menu.color);
	_zoe_updateObject(*id, map.line + frameNbLines - 1);
	// Read selection
	_zoe_setTerminalMode(MODE_RAW);
	int selection = 0;
	HIGHLIGHT(selection);
	HIDE_CURSOR();
	boolean help_is_displayed = FALSE;
	objectID idhelp;
	while (1) {
		int n = _zoe_incomingKey();
		if (help_is_displayed) {
			_zoe_clearObject(idhelp);
			help_is_displayed = FALSE;
		}
		switch (n) {
		case _back_previous_question:
			selection = ZOE_BACK;
			break;
		case _abort_session:
			selection = ZOE_ABORT;
			break;
		case _arrow_up:
			UNHIGHLIGHT(selection);
			if (selection == 0)
				selection = (int) nvalues - 1;
			else
				--selection;
			HIGHLIGHT(selection);
			break;
		case _arrow_down:
			UNHIGHLIGHT(selection);
			if (selection == (int) nvalues - 1)
				selection = 0;
			else
				++selection;
			HIGHLIGHT(selection);
			break;
		case _help:
			_zoe_help(&idhelp, map.helpline, map.helpstartcolumn, map.helpendcolumn, 1, (char *) help_menu);
			help_is_displayed = TRUE;
			break;
		case _ok:
			n = ZOE_OK;
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			if (n <= (int)nvalues) {
				UNHIGHLIGHT(selection);
				selection = n - 1;
				HIGHLIGHT(selection);
			} else
				DISPLAY_BELL();
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (n == ZOE_OK || selection == ZOE_ABORT || selection == ZOE_BACK) break;
	}
	SHOW_CURSOR();
	_zoe_setTerminalMode(MODE_ECHOED);
	EXIT_ZOE(map.line + frameNbLines);
	return selection;
}
//==============================================================================
