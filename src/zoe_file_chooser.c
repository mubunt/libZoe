//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct	tree_scanner *tree = NULL;
static size_t	width_of_the_widget = 0;
static size_t	number_of_items_that_can_be_displayed = 0;
static int		index_of_the_first_item_displayed = 0;
static int		line_of_the_first_item_displayed = 0;
static char		buffer[MAXLINE + PATH_MAX];
static char		buftmp[MAXLINE];
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define GET_TREE_ITEM(n)				tree->ptObjects[index_of_the_first_item_displayed + (int) n]
#define GET_MAX_LENGTH_OF_TREE_ITEMS()	tree->maxVisibleLenObjects
#define GET_NUMBER_OF_TREE_ITEMS()		tree->nbObjects
#define	GET_LENGTH_OF_QUESTION()		strlen(question)
#define GET_LENGTH_OF_ITEMS()			(width_of_the_widget - 9)

#define HIGHLIGHT(n)					sprintf(buffer, START_COLOR "%s %s %s%s" STOP_COLOR START_COLOR " %s%s %s" STOP_COLOR, \
    										attributs.chooser.bold, attributs.chooser.color, C_V, C_V, REVERSE, \
    										_completeLabel(GET_TREE_ITEM(n), GET_LENGTH_OF_ITEMS(), buftmp), \
    										attributs.chooser.bold, attributs.chooser.color, SCROLL, C_V, C_V); \
										_zoe_recordScreenmap(idmenu, line_of_the_first_item_displayed + n, map.menucolumn, buffer, TODISPLAY);

#define UNHIGHLIGHT(n)					sprintf(buffer, START_COLOR "%s %s %s %s%s %s" STOP_COLOR, \
											attributs.chooser.bold, attributs.chooser.color, C_V, C_V, \
											_completeLabel(GET_TREE_ITEM(n), GET_LENGTH_OF_ITEMS(), buftmp), \
											SCROLL, C_V, C_V); \
										_zoe_recordScreenmap(idmenu, line_of_the_first_item_displayed + n, map.menucolumn, buffer, TODISPLAY);

#define GOTONEXTLEVEL()					_zoe_clearObject(idmenu); \
										_getFiles(path, _answer, type, suffix); \
										_zoe_map_chooser(line, column, GET_LENGTH_OF_QUESTION(), &number_of_items_that_can_be_displayed, \
											width_of_the_widget, help_chooser, &map); \
										idmenu = _displayMenu(map.menuline, map.menucolumn, _answer); \
										selection = 0; \
										HIGHLIGHT(selection);

#define GOTOUPLEVEL()					_zoe_clearObject(idmenu); \
										_subPath(_answer); \
										_getFiles(path, _answer, type, suffix); \
										_zoe_map_chooser(line, column, GET_LENGTH_OF_QUESTION(), &number_of_items_that_can_be_displayed, \
											width_of_the_widget, help_chooser, &map); \
										idmenu = _displayMenu(map.menuline, map.menucolumn, _answer); \
										selection = 0; \
										HIGHLIGHT(selection);

#define REMOVE_ENDING_SLASH(s)		char *pt = s + strlen(s) - 1; if (*pt == '/') *pt = '\0';

#define CONCAT(buf, s1, s2, s3, s4)	strcat(buf, s1); strcat(buf, s2); strcat(buf, s3); strcat(buf, s4);
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void _addPath( char *dest, char *src ) {
	if (dest[strlen(dest) - 1] != '/') strcat(dest, "/");
	strcat(dest, src);
}

static void _subPath( char *dest ) {
	char *pt = dest + strlen(dest) - 1;
	if (*pt == '/') --pt;
	while (*pt != '/' && pt != dest) --pt;
	*++pt = '\0';
}

static char *_completeLabel( const char *str, size_t width, char *buf ) {
	strcpy(buf, str);
	for (size_t i = _zoe_visiblestrlen(str); i < width; i++) strcat(buf, " ");
	return buf;
}

static void _getFiles( const char *root, const char *path, zoe_choose type, const char *suffix[]) {
	if (tree != NULL) _zoe_free_treeScanner(tree);
	tree = _zoe_treeScanner(root, path, type, suffix, FALSE);
	number_of_items_that_can_be_displayed = GET_NUMBER_OF_TREE_ITEMS();
	width_of_the_widget = MAX(_zoe_visiblestrlen(path), GET_MAX_LENGTH_OF_TREE_ITEMS() + 5) + 4;
}

static void _displayTop( objectID id, int *line, int column, size_t width ) {
	++frameNbLines;
	sprintf(buffer, START_COLOR "%s %s", attributs.chooser.bold, attributs.chooser.color, C_V, C_ULC);
	for (size_t i = 0; i < (width - 6); i++) strcat(buffer, C_H);
	CONCAT(buffer, C_URC, " ", C_V, STOP_COLOR);
	_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
}

static void _displayBottom( objectID id, int *line, int column, size_t width ) {
	++frameNbLines;
	sprintf(buffer, START_COLOR "%s %s", attributs.chooser.bold, attributs.chooser.color, C_V, C_BLC);
	for (size_t i = 0; i < (width - 6); i++) strcat(buffer, C_H);
	CONCAT(buffer, C_BRC, " ", C_V, STOP_COLOR);
	_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
}

static void _displayLabel( objectID id, int *line, int column, size_t width, const char *label ) {
	++frameNbLines;
	sprintf(buffer, START_COLOR "%s %s %s" STOP_COLOR,
	        attributs.chooser.bold, attributs.chooser.color,
	        C_V, _completeLabel(label, (width - 4), buftmp), C_V);
	_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
}

static void _displayItem( objectID id, int *line, int column, size_t width, const char *label ) {
	++frameNbLines;
	sprintf(buffer, START_COLOR "%s %s %s %s%s %s" STOP_COLOR,
	        attributs.chooser.bold, attributs.chooser.color,
	        C_V, C_V, _completeLabel(label, GET_LENGTH_OF_ITEMS(), buftmp),
	        SCROLL, C_V, C_V);
	_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
}

static objectID _displayMenu( int line, int column, const char *path ) {
	objectID id;
	id = _zoe_newObject(line, column, 0, column + (int) width_of_the_widget + 1);
	_zoe_displayTop(id, &line, column, width_of_the_widget, attributs.chooser.bold, attributs.chooser.color, (char *) "");
	_displayLabel(id, &line, column, width_of_the_widget, path);
	_displayTop(id, &line, column, width_of_the_widget);
	index_of_the_first_item_displayed = 0;
	int lline = line_of_the_first_item_displayed = line;
	for (size_t i = 0; i < number_of_items_that_can_be_displayed; i++)
		_displayItem(id, &lline, column, width_of_the_widget, GET_TREE_ITEM(i));
	_displayBottom(id, &lline, column, width_of_the_widget);
	_zoe_displayBottom(id, lline, column, width_of_the_widget, attributs.chooser.bold, attributs.chooser.color);
	FLUSH();
	_zoe_updateObject(id, line + frameNbLines - 1);
	return id;
}
//------------------------------------------------------------------------------
// ZOE API FUNCTIONS
//------------------------------------------------------------------------------
int zoe_file_chooser( objectID *id, int line, int column,
                      const char *question, const char *path, zoe_choose type, const char *suffix[],
                      char *answer ) {
	struct mapping_close map;
	objectID idmenu;
	char _answer[PATH_MAX];

	ENTER_ZOE();
	// Get directories and files,
	tree = NULL;
	_getFiles(path, path, type, suffix);
	// Position of the question / answer / menu / help
	_zoe_map_chooser(line, column, GET_LENGTH_OF_QUESTION(), &number_of_items_that_can_be_displayed, width_of_the_widget, (char *)help_chooser, &map);
	// Get object id.
	*id = _zoe_newObject(map.line, column, map.line, column + (int) (GET_LENGTH_OF_QUESTION() + GET_MAX_LENGTH_OF_TREE_ITEMS() + 1));
	// Display the question and the area for the answer
	sprintf(buffer, FORMAT_MOVE_CURSOR_AND_CLEAR, map.line, column);
	_zoe_recordScreenmap(*id, map.line, column, buffer, TODISPLAY);
	sprintf(buffer, START_COLOR "%s ", attributs.question.bold, attributs.question.color, question);
	for (size_t i = 0; i < GET_MAX_LENGTH_OF_TREE_ITEMS(); i++)
		strcat(buffer, ANSWER);
	strcat(buffer, STOP_COLOR);
	_zoe_recordScreenmap(*id, map.line, column, buffer, TODISPLAY);
	// Display the list of files or directories
	idmenu = _displayMenu(map.menuline, map.menucolumn, path);
	// Highlight first value
	int selection = 0;
	HIGHLIGHT(selection);
	// Read selection
	_zoe_setTerminalMode(MODE_RAW);
	HIDE_CURSOR();
	strcpy(_answer, path);
	int status;
	boolean help_is_displayed = FALSE;
	objectID idhelp;
	while (1) {
		int n = _zoe_incomingKey();
		if (help_is_displayed) {
			_zoe_clearObject(idhelp);
			help_is_displayed = FALSE;
		}
		switch (n) {
		case _abort_session:
			strcpy(_answer, "");
			status = ZOE_ABORT;
			break;
		case _back_previous_question:
			strcpy(_answer, "");
			status = ZOE_BACK;
			break;
		case _arrow_up:
			if (selection == 0) {
				if (index_of_the_first_item_displayed == 0)
					DISPLAY_BELL();
				else  {
					UNHIGHLIGHT(selection);
					--index_of_the_first_item_displayed;
					int lline = line_of_the_first_item_displayed;
					size_t  i;
					for (i = 0; i < number_of_items_that_can_be_displayed; i++)
						_displayItem(idmenu, &lline, map.menucolumn, width_of_the_widget, GET_TREE_ITEM(i));
					HIGHLIGHT(selection);
				}
			} else {
				UNHIGHLIGHT(selection);
				--selection;
				HIGHLIGHT(selection);
			}
			status = _goon;
			break;
		case _arrow_down:
			if (selection == (int) number_of_items_that_can_be_displayed - 1) {
				if ((index_of_the_first_item_displayed + selection) == (int) GET_NUMBER_OF_TREE_ITEMS() - 1)
					DISPLAY_BELL();
				else  {
					UNHIGHLIGHT(selection);
					++index_of_the_first_item_displayed;
					int lline = line_of_the_first_item_displayed;
					size_t  i;
					for (i = 0; i < number_of_items_that_can_be_displayed; i++)
						_displayItem(idmenu, &lline, map.menucolumn, width_of_the_widget, GET_TREE_ITEM(i));
					selection = (int) i - 1;
					HIGHLIGHT(selection);
				}
			} else {
				UNHIGHLIGHT(selection);
				++selection;
				HIGHLIGHT(selection);
			}
			status = _goon;
			break;
		case _go_next_question:
			if (strcmp(GET_TREE_ITEM(selection), UPDIR) == 0) {
				GOTOUPLEVEL();
			} else {
				_addPath(_answer, GET_TREE_ITEM(selection));
				if (_zoe_isDirectory(_answer)) {
					GOTONEXTLEVEL();
				} else {
					_subPath(_answer);
					DISPLAY_BELL();
				}
			}
			status = _goon;
			break;
		case _ok:
			if (strcmp(GET_TREE_ITEM(selection), UPDIR) == 0) {
				GOTOUPLEVEL();
				status = _goon;
			} else {
				_addPath(_answer, GET_TREE_ITEM(selection));
				if (_zoe_isDirectory(_answer)) {
					if (type == CHOOSE_DIR) {
						REMOVE_ENDING_SLASH(_answer);
						status = ZOE_OK;
					} else {
						GOTONEXTLEVEL();
						status = _goon;
					}
				} else
					status = ZOE_OK;
			}
			break;
		case _help:
			_zoe_help(&idhelp, map.helpline, map.helpstartcolumn, map.helpendcolumn, 1, (char *) help_chooser);
			help_is_displayed = TRUE;
			status = _goon;
			break;
		default:
			DISPLAY_BELL();
			status = _goon;
			break;
		}
		if (status == ZOE_OK || status == ZOE_ABORT || status == ZOE_BACK) break;
	}
	SHOW_CURSOR();
	_zoe_setTerminalMode(MODE_ECHOED);
	strcpy(answer, _answer);
	// Clear menu
	_zoe_clearObject(idmenu);
	if (status == ZOE_OK) {
		sprintf(buffer, START_COLOR "%s" STOP_COLOR, attributs.answer.bold, attributs.answer.color, answer);
		_zoe_recordScreenmap(*id, map.line, map.answercolumn, buffer, TODISPLAY);
	}
	if (status == ZOE_BACK)
		_zoe_clearObject(*id);
	_zoe_free_treeScanner(tree);
	_zoe_refresh();
	EXIT_ZOE(map.line + 1);
	return status;
}
//==============================================================================
