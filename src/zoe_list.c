//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// CONSTANTS
//------------------------------------------------------------------------------
#define BACK 	"BACK"
#define OK 		"OK"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static char nilhighlighted[MAXLINE];
static char okhighlighted[MAXLINE];
static char backhighlighted[MAXLINE];
static int linehighlighted;
static int columnhighlighted;
static int firstlineoflist;
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define INITFIELD(field, a)		do { \
									size_t p = (bwidth - strlen(a) - 2) / 2;  \
									strcpy(field, " ");  \
									for (size_t i = 0; i < p; i++) strcat(field, " "); \
									strcat(field, a); \
									for (size_t i = 0; i < bwidth - strlen(a) - p - 2; i++) strcat(field, " "); \
									strcat(field, " "); \
								} while (0);
#define HIGHLIGHT(n)			sprintf(buffer, START_COLOR "%s [ %c ] %s%-*s" STOP_COLOR START_COLOR " %s" STOP_COLOR, \
									attributs.list.bold, attributs.list.color, C_V, (list[n - firstlineoflist].requested ? 'X' : ' '), \
									REVERSE, (int) width - 10, list[n - firstlineoflist].label, attributs.list.bold, attributs.list.color, C_V); \
								_zoe_recordScreenmap(*id, n, map.column, buffer, TODISPLAY);
#define UNHIGHLIGHT(n)			sprintf(buffer, START_COLOR "%s [ %c ] %-*s %s" STOP_COLOR, \
									attributs.list.bold, attributs.list.color, C_V, (list[n - firstlineoflist].requested ? 'X' : ' '), \
									(int) width - 10, list[n - firstlineoflist].label, C_V); \
								_zoe_recordScreenmap(*id, n, map.column, buffer, TODISPLAY);
#define HIGHLIGHTCHOICE()		if (choice) sprintf(buffer, "%s", okhighlighted); \
								else sprintf(buffer, "%s", backhighlighted); \
								_zoe_recordScreenmap(*id, linehighlighted, columnhighlighted, buffer, TODISPLAY);
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void displaylist( objectID id, int *line, int column, size_t width, struct zoe_list_item list[], size_t nitems ) {
	char buffer[MAXLINE];
	frameNbLines += (int) nitems;
	firstlineoflist = *line;
	for (size_t i = 0; i < nitems; i++) {
		sprintf(buffer, START_COLOR "%s [ %c ] %-*s %s" STOP_COLOR,
		        attributs.list.bold, attributs.list.color, C_V, (list[i].requested ? 'X' : ' '), (int)width - 10, list[i].label, C_V);
		_zoe_recordScreenmap(id, (*line)++, column, buffer, TODISPLAY);
	}
}
//------------------------------------------------------------------------------
static void displaychoice( objectID id, int *line, int column, size_t width, size_t bwidth ) {
	char buf[16];
	sprintf(buf, STOP_COLOR START_COLOR, attributs.list.bold, attributs.list.color);
	linehighlighted = *line;
	columnhighlighted = column;
	char *field1 = _zoe_malloc(bwidth + 1, sizeof(char));
	char *field2 = _zoe_malloc(bwidth + 1, sizeof(char));
	INITFIELD(field1, BACK);
	INITFIELD(field2, OK);
	int p = (int) ((width - 2) - (2 * bwidth));

	sprintf(nilhighlighted, START_COLOR "%s", attributs.list.bold, attributs.list.color, C_V);
	for (int i = 0; i < p / 3 ; i++) strcat(nilhighlighted, " ");
	strcat(nilhighlighted, field1);
	for (int i = 0; i < p - 2 * (p / 3); i++) strcat(nilhighlighted, " ");
	strcat(nilhighlighted, field2);
	for (int i = 0; i < p / 3 ; i++) strcat(nilhighlighted, " ");
	strcat(nilhighlighted, C_V);
	strcat(nilhighlighted, STOP_COLOR);

	sprintf(backhighlighted, START_COLOR "%s", attributs.list.bold, attributs.list.color, C_V);
	for (int i = 0; i < p / 3 ; i++) strcat(backhighlighted, " ");
	strcat(backhighlighted, REVERSE);
	strcat(backhighlighted, field1);
	strcat(backhighlighted, buf);
	for (int i = 0; i < p - 2 * (p / 3); i++) strcat(backhighlighted, " ");
	strcat(backhighlighted, field2);
	for (int i = 0; i < p / 3 ; i++) strcat(backhighlighted, " ");
	strcat(backhighlighted, C_V);
	strcat(backhighlighted, STOP_COLOR);

	sprintf(okhighlighted, START_COLOR "%s", attributs.list.bold, attributs.list.color, C_V);
	for (int i = 0; i < p / 3 ; i++) strcat(okhighlighted, " ");
	strcat(okhighlighted, field1);
	for (int i = 0; i < p - 2 * (p / 3); i++) strcat(okhighlighted, " ");
	strcat(okhighlighted, REVERSE);
	strcat(okhighlighted, field2);
	strcat(okhighlighted, buf);
	for (int i = 0; i < p / 3 ; i++) strcat(okhighlighted, " ");
	strcat(okhighlighted, C_V);
	strcat(okhighlighted, STOP_COLOR);

	_zoe_free(field1);
	_zoe_free(field2);
	++frameNbLines;
	_zoe_recordScreenmap(id, (*line)++, column, nilhighlighted, TODISPLAY);
}
//------------------------------------------------------------------------------
static void displayall(objectID id, struct mapping_generic map, const char *question, struct zoe_list_item list[], size_t nitems,
                       size_t width, size_t buttonwidth) {
	int lline = map.line;
	_zoe_displayTop(id, &lline, map.column, width, attributs.list.bold, attributs.list.color, (char *) "");
	_zoe_displayLabel(id, &lline, map.column, width - 4, question, attributs.list.bold, attributs.list.color);
	_zoe_displayEmptyLine(id, &lline, map.column, width, attributs.list.bold, attributs.list.color);
	displaylist(id, &lline, map.column,  width, list, nitems);
	_zoe_displayEmptyLine(id, &lline, map.column, width, attributs.list.bold, attributs.list.color);
	displaychoice(id, &lline, map.column,  width, buttonwidth);
	_zoe_displayBottom(id, lline, map.column, width, attributs.list.bold, attributs.list.color);
	FLUSH();
}
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
int zoe_list( objectID *id, int line, int column, const char *question, struct zoe_list_item list[], size_t nitems ) {
	struct mapping_generic map;
	char buffer[MAXLINE];
	ENTER_ZOE();
	// Size of the longest value
	size_t labelwidth = 0;
	for (size_t i = 0; i < nitems; i++) labelwidth = MAX(labelwidth, strlen(list[i].label));
	// The width of the menu is the largest between the widths of the values plus selector area (6 characters),
	// the question, and the button line, plus the edges (2 * 2 characters)
	size_t questionwidth = MAX(strlen(question), labelwidth + 6);
	size_t buttonwidth = 4 + MAX(strlen(BACK), strlen(OK));
	size_t width =  4 + MAX(questionwidth, 2 * buttonwidth + 2);
	// Position of the question / answer / menu / help
	_zoe_map_list(line, column, nitems, (char *)help_list, &map);
	// Get object id.
	*id = _zoe_newObject(map.line, map.column, 0, map.column + (int) width + 1);
	// Display the list
	displayall(*id, map, question, list, nitems,  width, buttonwidth);
	_zoe_updateObject(*id, map.line + frameNbLines - 1);
	// Read selection
	_zoe_setTerminalMode(MODE_RAW);
	HIDE_CURSOR();
	boolean help_is_displayed = FALSE;
	objectID idhelp;
	boolean choice = FALSE;
	int l, c;
	int selection = firstlineoflist;
	HIGHLIGHT(selection);
	while (1) {
		int n = _zoe_incomingKey();
		if (help_is_displayed) {
			_zoe_clearObject(idhelp);
			help_is_displayed = FALSE;
		}
		switch (n) {
		case _abort_session:
			selection = ZOE_ABORT;
			break;
		case _arrow_up:
			if (selection == linehighlighted) {
				_zoe_recordScreenmap(*id, linehighlighted, columnhighlighted, nilhighlighted, TODISPLAY);
				selection = firstlineoflist + (int) nitems - 1;
				HIGHLIGHT(selection);
			} else {
				UNHIGHLIGHT(selection);
				if (selection == firstlineoflist) {
					selection = linehighlighted;
					HIGHLIGHTCHOICE();
				} else {
					--selection;
					HIGHLIGHT(selection);
				}
			}
			break;
		case _arrow_down:
			if (selection == linehighlighted) {
				_zoe_recordScreenmap(*id, linehighlighted, columnhighlighted, nilhighlighted, TODISPLAY);
				selection = firstlineoflist;
				HIGHLIGHT(selection);
			} else {
				UNHIGHLIGHT(selection);
				if (selection == firstlineoflist + (int) nitems - 1) {
					selection = linehighlighted;
					HIGHLIGHTCHOICE();
				} else {
					++selection;
					HIGHLIGHT(selection);
				}
			}
			break;
		case _go_next_question:
			if (selection == linehighlighted) {
				if (choice)
					DISPLAY_BELL();
				else {
					choice = TRUE;
					_zoe_recordScreenmap(*id, linehighlighted, columnhighlighted, okhighlighted, TODISPLAY);
				}
			} else
				DISPLAY_BELL();
			break;
		case _back_previous_question:
			if (selection == linehighlighted) {
				if (choice) {
					choice = FALSE;
					_zoe_recordScreenmap(*id, linehighlighted, columnhighlighted, backhighlighted, TODISPLAY);
				} else
					DISPLAY_BELL();
			} else
				DISPLAY_BELL();
			break;
		case _help:
			_zoe_help(&idhelp, map.helpline, map.helpstartcolumn, map.helpendcolumn, 1, (char *) help_list);
			help_is_displayed = TRUE;
			break;
		case _ok:
			if (selection == linehighlighted) {
				HIGHLIGHTCHOICE();
				if (choice)
					selection = ZOE_OK;
				else
					selection = ZOE_BACK;
			} else {
				if (list[selection - firstlineoflist].requested)
					list[selection - firstlineoflist].requested = FALSE;
				else
					list[selection - firstlineoflist].requested = TRUE;
				HIGHLIGHT(selection);
			}
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (selection == ZOE_OK || selection == ZOE_BACK || selection == ZOE_ABORT) break;
	}
	SHOW_CURSOR();
	_zoe_setTerminalMode(MODE_ECHOED);
	if (selection == ZOE_BACK) _zoe_clearObject(*id);
	EXIT_ZOE(map.line + frameNbLines);
	return selection;
}
//==============================================================================
