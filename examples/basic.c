//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <termcap.h>
#include <stdarg.h>
#include <sys/ioctl.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe.h"
//------------------------------------------------------------------------------
// MACRO_CODES
//------------------------------------------------------------------------------
#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature", "Fifth feature",
	                           "Sixth feature", "Seventh feature", "Eighth feature", "Ninth feature", "Tenth feature",
	                           "Eleventh feature", "Twelfth feature", "Thirteenth feature", "Fourteenth feature", "Fifteenth feature"
	                         };
	const char *colors[] = { "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white", "normal" };
	const char *attribut[] = { "bold", "boldunderline", "underline", "italic", "normal" };
	const char *help = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna" \
	                   "aliquyam-erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                   "diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit X.";

	const char *error = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna" \
	                    "aliquyam erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                    "diam voluptua.";
	const char *message = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna " \
	                      "aliquyam-erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                      "diam voluptua.\n" \
	                      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna" \
	                      "aliquyam-erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                      "diam voluptua.";
	const char *bomberman = \
	                        "                          .-. \n" \
	                        "                          '-' \n" \
	                        "                         // \n" \
	                        "                _..---._/| \n" \
	                        "              .' .\"     '-. \n" \
	                        "             /__/          \\      * \n" \
	                        "            ====_____     __|     : \n" \
	                        "           /#   #\"\"\" |   /()\\    :    ..* \n" \
	                        "           |#   #    |   \\__/    : .''  \n" \
	                        "           \\#___#____|      /   :::.. . \n" \
	                        "            \\______________|_...‰_: .. '* \n" \
	                        "   ()       // /\\||||)))))))      '   . . \n" \
	                        "  .( \\_     \\\\_\\//   _-'.'/        |   * .. \n" \
	                        " ( )  |^|^|^|ooo/  _#\\.//\"\"\"_      |   . . . \n" \
	                        " (_)_.'v|v|v|     / \\#  \\_ / '_  _'    . .   \n" \
	                        "            | _ _/_/     /'./_-|\"         . . \n" \
	                        "            /#_#__\"\"\"-._ /#  \\__)       .  .    \n" \
	                        "            |__   \"\"-._ |##               . . . \n" \
	                        "            |  \"\"|-\"\"\"-_/##              . .     \n" \
	                        "            /\"\"--\\__.-|                       . \n" \
	                        "            \\-_.-<__ /                   .   . \n" \
	                        "            /-_| /  \\ \n" \
	                        "            \\-_| \\_-<.                        .  . \n" \
	                        "            <_-/ <_.-\\                    . \n" \
	                        "            <_-|  |_.-|                        . \n" \
	                        "       .----|   \\__\\  |                 . \n" \
	                        "      |     .\"\"\"   '.  |                       . \n" \
	                        "       .___|        |__|  (c) NamelessOne \n" \
	                        "           '.__...\"\"\"  ";

	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};
	const char *suffix[] = { "o", "a", "\0"};
	//--------------------------------------------------------------------------
	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);

	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
	char buf[64];

Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is: %s\n", features[i]);

	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is: %s\n", colors[i]);
Q2:
	i = zoe_close_question(&id, 2, 39, "Which attribut?     ",  attribut, 5);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is: %s\n", attribut[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", help, answer, 250);
	ZOE_REPORT(i, ABORT, Q2)
	fprintf(stdout, "Answer is: %s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
Q5:
	i = zoe_file_chooser(&id, 10, 63, "Choose a directory....", ".", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q4)
	fprintf(stdout, "Answer is: %s\n", answer);
Q6:
	i = zoe_file_chooser(&id, 11, 63, "Choose a file....", ".", CHOOSE_FILE, NULL, answer);
	ZOE_REPORT(i, ABORT, Q5)
	fprintf(stdout, "Answer is: %s\n", answer);
Q7:
	zoe_progressbar(&id, message);
	for (int i = 1; i <= 10; i++) {
		zoe_progressbar_display(id, i * 10);
		sleep(1);
	}
	zoe_clearObject(id);

Q8:
	b = zoe_gonogo(&id, "Go on with the file generation?", "Yes", "No");
	fprintf(stdout, "Answer is: %d\n", b);
Q9:
	zoe_message(message);
Q10:
	for (i = 5; i > 0; i--) {
		sprintf(buf, "The result will appear in %d seconds ....", i);
		zoe_information(&id, buf);
		sleep(1);
		zoe_clearObject(id);
	}
	zoe_message(bomberman);
	zoe_error(error);
	zoe_error("Another error, but shorter....");

	fprintf(stdout, "End.....\n");

	sleep(5);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
//==============================================================================
